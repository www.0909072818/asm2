﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Model
{
    [BsonIgnoreExtraElements]
    public class Branch
    {
        [BsonId] 
        public Guid Id { get; set; }
        [BsonElement("BranchId")]
        public string ?BranchId { get; set; }
        [BsonElement("Name")] 
        public string ?Name { get; set; }
        [BsonElement("Address")] 
        public string ?Address { get; set; }
        [BsonElement("City")] 
        public string ?City { get; set; }
        [BsonElement("State")] 
        public string ?State { get; set; }
        [BsonElement("ZipCode")] 
        public string ?ZipCode { get; set; }
    }
}
