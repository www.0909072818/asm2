﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using Repository.Model;
using Repository.ViewModel;

namespace MongDB_Test_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private IMongoCollection<Branch> _branchCollection;
        public BranchesController(IMongoClient client)
        {
            var database = client.GetDatabase("CustomerDB");
            _branchCollection = database.GetCollection<Branch>("Branch");
        }
        [HttpGet("GetAllBranch")]
        public IActionResult getall()
        {
            List<Branch> branchs = _branchCollection.Find(_ => true).ToList();
            return Ok(branchs);
        }
        [HttpPost("Add New Branch")]
        public IActionResult Add(BranchViewModel branchview)
        {
            Branch branch = new Branch
            {
                Id = new Guid(),
                BranchId = branchview.BranchId,
                Name = branchview.Name,
                Address = branchview.Address,
                City = branchview.City,
                State = branchview.State,
                ZipCode = branchview.ZipCode,
            };
            _branchCollection.InsertOne(branch);
            return Ok("Branch added successfully.");
        }
        [HttpDelete("DelelteByBranchID/{id}")]
        public IActionResult DeleteById(Guid id)
        {
            var filter = Builders<Branch>.Filter.Eq("_id", id);
            var result = _branchCollection.DeleteOne(filter);

            if (result.DeletedCount == 0)
            {
                return NotFound("Branch not found.");
            }
            return Ok("Branch deleted successfully.");
        }
        [HttpPut("UpdateByBranchId/{id}")]
        public IActionResult UpdateByBranchID(Guid id, BranchViewModel branchView)
        {
            var filter = Builders<Branch>.Filter.Eq("_id", id);
            var update = Builders<Branch>.Update
                .Set("Name", branchView.Name)
                .Set("Address", branchView.Address)
                .Set("City", branchView.City)
                .Set("State", branchView.State)
                .Set("ZipCode", branchView.ZipCode);

            var options = new UpdateOptions { IsUpsert = false };

            var result = _branchCollection.UpdateOne(filter, update, options);

            if (result.ModifiedCount == 0)
            {
                return NotFound("Branch not found.");
            }
            return Ok("Branch updated successfully.");
        }
    }
}